/*
 * Copyright (c) 2017, orillion <orillion@rizon.net>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package net.rizon.acid.plugins.vizon;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

/**
 *
 * @author orillion <orillion@rizon.net>
 */
public class DrawGeneratorTest
{
	private DrawGenerator generator;

	public DrawGeneratorTest()
	{
	}

	@Before
	public void setup()
	{
		generator = new DrawGenerator();
	}

	/**
	 * Test of generate method, of class DrawGenerator.
	 */
	@Test
	public void generateTest()
	{
		Bet bet = generator.generate();
		Assert.assertNotNull(bet);
	}

	@Ignore("Testing randomness")
	@Test
	public void generate1000Test()
	{
		int[] draws = new int[29];

		for (int i = 0; i < 10000; i++)
		{
			Bet bet = generator.generate();

			draws[bet.getFirst() - 1]++;
			draws[bet.getSecond() - 1]++;
			draws[bet.getThird() - 1]++;
			draws[bet.getFourth() - 1]++;
			draws[bet.getFifth() - 1]++;
			draws[bet.getSixth() - 1]++;
		}

		for (int i = 0; i < 29; i++)
		{
			int draw = draws[i];
			System.out.println(String.format("Number %2d was drawn %d times", i + 1, draw));
		}
	}

	@Ignore("Testing randomness")
	@Test
	public void bet123456Test()
	{
		Bet bet = new Bet(1, 2, 3, 4, 5, 6);

		long totalRuns = 0;
		int cycles = 100;

		Bet draw;

		for (int i = 0; i < cycles; i++)
		{
			int runs = 0;

			do
			{
				draw = generator.generate();
				runs++;
			}
			while (!draw.hasSameNumbers(bet));

			totalRuns += runs;

			System.out.println(String.format("Run %3d completed", i + 1));
		}

		System.out.println(String.format("It took an average of %d runs to win first prize with bet 1 2 3 4 5 6", totalRuns / cycles));
	}

	/**
	 * Test of generateSpecial method, of class DrawGenerator.
	 */
	@Test
	public void testGenerateSpecial()
	{

	}

}
