/*
 * Copyright (c) 2017, Orillion <orillion@rizon.net>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package net.rizon.acid.plugins.vizon.commands;

import java.util.Collections;
import java.util.List;
import net.rizon.acid.core.AcidUser;
import net.rizon.acid.core.Acidictive;
import net.rizon.acid.core.Channel;
import net.rizon.acid.core.Command;
import net.rizon.acid.core.User;
import net.rizon.acid.plugins.vizon.Vizon;
import net.rizon.acid.plugins.vizon.db.VizonDrawing;
import net.rizon.acid.util.Format;

/**
 *
 * @author Orillion <orillion@rizon.net>
 */
public class OrderResultsCommand extends Command
{

	public OrderResultsCommand()
	{
		super(0, 0);
	}

	@Override
	public void Run(User source, AcidUser to, Channel c, String[] args)
	{
		Acidictive.reply(source, to, c, "Updating records...");

		List<VizonDrawing> drawings = Vizon.getVizonDatabase().findAllRunDrawings();

		for (VizonDrawing drawing : drawings)
		{
			List<Integer> winning = drawing.getDraws();
			Collections.sort(winning);
			drawing.setDrawingResult(winning);

			Vizon.getVizonDatabase().updateDrawing(drawing);
		}

		Acidictive.reply(source, to, c, String.format(
				"Updated %s%d%s records",
				Format.color(Format.ORANGE),
				drawings.size(),
				Format.color(0)));
	}

	@Override
	public void onHelp(User u, AcidUser to, Channel c)
	{
		Acidictive.reply(u, to, c, "\002ORDER\002 Orders all drawing results from low to high");
	}

	@Override
	public boolean onHelpCommand(User u, AcidUser to, Channel c)
	{
		Acidictive.reply(u, to, c, "Syntax: \002ORDER\002");
		Acidictive.reply(u, to, c, " ");
		Acidictive.reply(u, to, c, "Orders all drawing results from low to high");

		return true;
	}

}
