package net.rizon.acid.plugins.trapbot;

import java.util.concurrent.TimeUnit;
import net.rizon.acid.core.Acidictive;
import net.rizon.acid.core.Message;
import net.rizon.acid.core.Protocol;

class ReleaseTimer implements Runnable
{
	@Override
	public void run()
	{
		Protocol.privmsg(trapbot.trapbot.getUID(), trapbot.getTrapChanName(), Message.BOLD + "YOU HAVE 1 MINUTE TO PART THE CHANNEL, GET OUT WHILE YOU CAN!" + Message.BOLD);

		trapbot.enforce = false;
		trapbot.releaseTimer = null;

		trapbot.retrapTimer = Acidictive.schedule(new RetrapTimer(), 1, TimeUnit.MINUTES);
	}
}
