package net.rizon.acid.core;

public enum ChannelStatus
{
	VOICE("v"),
	HALFOP("h"),
	OP("o"),
	ADMIN("a"),
	OWNER("q");

	private final String modeChar;

	ChannelStatus(String modeChar)
	{
		this.modeChar = modeChar;
	}

	@Override
	public String toString()
	{
		return this.modeChar;
	}

	public static ChannelStatus fromString(String c)
	{
		for (ChannelStatus m : ChannelStatus.values())
		{
			if (m.modeChar.equals(c))
			{
				return m;
			}
		}

		throw new IllegalArgumentException("No constant with text " + c + " found");
	}
}
