package net.rizon.acid.core;

public class ChannelModeChange
{
	private User user;
	private ChannelStatus mode;
	private boolean given;

	public ChannelModeChange(User user, ChannelStatus mode, boolean given)
	{
		this.user = user;
		this.mode = mode;
		this.given = given;
	}

	public User getUser()
	{
		return user;
	}

	public ChannelStatus getMode()
	{
		return mode;
	}

	public boolean isGiven()
	{
		return given;
	}
}
