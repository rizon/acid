package net.rizon.acid.conf;

public class PluginDesc
{
	private String groupId;
	private String artifactId;
	private String version;

	@Override
	public String toString()
	{
		return "PluginDesc{" + "groupId=" + groupId + ", artifactId=" + artifactId + ", version=" + version + '}';
	}

	public String getGroupId()
	{
		return groupId;
	}

	public void setGroupId(String groupId)
	{
		this.groupId = groupId;
	}

	public String getArtifactId()
	{
		return artifactId;
	}

	public void setArtifactId(String artifactId)
	{
		this.artifactId = artifactId;
	}

	public String getVersion()
	{
		return version;
	}

	public void setVersion(String version)
	{
		this.version = version;
	}
}
