package net.rizon.acid.arguments;

/**
 *
 * @author orillion
 */
public class LimitArgument
{
	private final int limit;
	private final String argument;

	private LimitArgument(String arg, int limit)
	{
		this.limit = limit;
		this.argument = arg;
	}

	/**
	 * Attempts to parse a limit argument. Limit arguments are in the form of
	 * +INT example: +100
	 *
	 * @param arg Argument to parse
	 *
	 * @return A {@link LimitArgument} or null if unparseable.
	 */
	public static LimitArgument parse(String arg)
	{
		int limit;

		if (arg == null || arg.isEmpty())
		{
			return null;
		}

		if (!arg.startsWith("+"))
		{
			return null;
		}

		try
		{
			limit = Integer.parseInt(arg.substring(1));
		}
		catch (NumberFormatException e)
		{
			return null;
		}

		if (limit <= 0)
		{
			return null;
		}

		return new LimitArgument(arg, limit);
	}

	public int getLimit()
	{
		return this.limit;
	}

	public String getArgument()
	{
		return this.argument;
	}
}
