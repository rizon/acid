/*
 * Copyright (c) 2016, Darius Jahandarie <djahandarie@rizon.net>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package net.rizon.acid.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import net.rizon.acid.util.Format;

public class Table
{
	private final String divider;
	private final List<Row> rows = new ArrayList();

	public Table()
	{
		this.divider = "";
	}

	public Table(String divider)
	{
		this.divider = divider;
	}

	public static class Row
	{
		private String wrap = "";
		private final List<String> cells;

		public Row(List<String> cells)
		{
			this.cells = cells;
		}

		public Row(String... cells)
		{
			this.cells = Arrays.asList(cells);
		}

		public Row setWrap(String wrap)
		{
			this.wrap = wrap;
			return this;
		}

		public String getWrap()
		{
			return wrap;
		}

		public List<String> getCells()
		{
			return cells;
		}
	}

	public void addHeader(String... columnTitles)
	{
		this.rows.add(0, new Row(columnTitles).setWrap("" + Format.BOLD));
	}

	public void addRow(String... cells)
	{
		this.rows.add(new Row(cells));
	}

	public void addRow(Row row)
	{
		this.rows.add(row);
	}


	public String render()
	{
		int[] colWidth = new int[this.rows.get(0).getCells().size()];
		for (Row row : this.rows)
		{
			List<String> cells = row.getCells();
			for (int col = 0; col < cells.size(); col++)
			{
				String cell = cells.get(col);
				if (cell == null)
				{
					cells.set(col, "");
				}
				else
				{
					colWidth[col] = Math.max(colWidth[col], Format.visibleLength(cell));
				}
			}
		}

		// generate output
		StringBuilder output = new StringBuilder();
		for (int r = 0; r < this.rows.size(); r++)
		{
			Row row = this.rows.get(r);
			List<String> cells = row.getCells();
			for (int c = 0; c < cells.size(); c++)
			{
				String cell = cells.get(c);

				output.append(row.getWrap());

				output.append(cell);

				char[] spaces = new char[colWidth[c] - Format.visibleLength(cell)];
				Arrays.fill(spaces, ' ');
				output.append(spaces);

				output.append(row.getWrap());

				if (c != cells.size() - 1)
				{
					output.append(" ");
					output.append(divider);
					output.append(" ");
				}
			}
			if (r != this.rows.size() - 1)
			{
				output.append("\n");
			}
		}
		return output.toString();
	}
}
