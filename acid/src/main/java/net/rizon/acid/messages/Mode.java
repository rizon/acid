package net.rizon.acid.messages;

import net.rizon.acid.core.Acidictive;
import net.rizon.acid.core.Message;
import net.rizon.acid.core.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Mode extends Message
{
	private static final Logger log = LoggerFactory.getLogger(Mode.class);

	public Mode()
	{
		super("MODE");
	}

	// :99hAAAAAB MODE 99hAAAAAB :-y

	@Override
	public void on(String source, String[] params)
	{
		if (params[0].startsWith("#")) // This isn't possible I guess?
			return;

		User setter = User.findUser(source), target = User.findUser(params[0]);
		if (setter == null)
		{
			log.warn("MODE from nonexistent source " + source);
			return;
		}
		else if (target == null)
		{
			log.warn("MODE for nonexistent user " + params[0]);
			return;
		}

		target.setMode(params[1]);
		Acidictive.onMode(setter.getNick(), target.getNick(), params[1]);
	}
}