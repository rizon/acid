/*
 * Copyright (C) 2017 Kristina Brooks. All rights reserved.
 *
 * Support for TS6 TMODE.
 * Format is :<SID|UID> TMODE <TS> <CHANNAME> <MODESTRING>
 */
package net.rizon.acid.messages;

import net.rizon.acid.core.Acidictive;
import net.rizon.acid.core.Channel;
import net.rizon.acid.core.Message;
import net.rizon.acid.core.Server;
import net.rizon.acid.core.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TMode extends Message
{
	private static final Logger log = LoggerFactory.getLogger(TMode.class);

	public TMode()
	{
		super("TMODE");
	}

	private void run(String source, String[] params)
	{
		Channel chan = Channel.findChannel(params[1]);
		if (chan == null || shouldDropTsMessage(chan.getTS(), params[0]))
		{
			return;
		}

		String modes = params[2];
		for (int i = 3; i < params.length; i++)
		{
			modes += " " + params[i];
		}

		Acidictive.onChanMode(
			source,
			chan,
			modes);

		if (chan.size() == 0 && !chan.hasMode('z'))
		{
			chan.destroy();
		}
	}

	@Override
	public void onUser(User source, String[] params)
	{
		run(source.getNick(), params);
	}

	@Override
	public void onServer(Server source, String[] params)
	{
		run(source.getName(), params);
	}
}
