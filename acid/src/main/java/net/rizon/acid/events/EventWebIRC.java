package net.rizon.acid.events;

import net.rizon.acid.core.Server;

public class EventWebIRC
{
	private Server source;
	private String operation;
	private String uid;
	private String realhost;
	private String sockhost;
	private String webircPassword;
	private String webircUsername;
	private String fakeHost;
	private String fakeIp;

	public Server getSource()
	{
		return source;
	}

	public void setSource(Server source)
	{
		this.source = source;
	}

	public String getOperation()
	{
		return operation;
	}

	public void setOperation(String operation)
	{
		this.operation = operation;
	}

	public String getUid()
	{
		return uid;
	}

	public void setUid(String uid)
	{
		this.uid = uid;
	}

	public String getRealhost()
	{
		return realhost;
	}

	public void setRealhost(String realhost)
	{
		this.realhost = realhost;
	}

	public String getSockhost()
	{
		return sockhost;
	}

	public void setSockhost(String sockhost)
	{
		this.sockhost = sockhost;
	}

	public String getWebircPassword()
	{
		return webircPassword;
	}

	public void setWebircPassword(String webircPassword)
	{
		this.webircPassword = webircPassword;
	}

	public String getWebircUsername()
	{
		return webircUsername;
	}

	public void setWebircUsername(String webircUsername)
	{
		this.webircUsername = webircUsername;
	}

	public String getFakeHost()
	{
		return fakeHost;
	}

	public void setFakeHost(String fakeHost)
	{
		this.fakeHost = fakeHost;
	}

	public String getFakeIp()
	{
		return fakeIp;
	}

	public void setFakeIp(String fakeIp)
	{
		this.fakeIp = fakeIp;
	}
}
