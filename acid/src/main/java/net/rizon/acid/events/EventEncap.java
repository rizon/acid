/*
 * Copyright (c) 2017, orillion <orillion@rizon.net>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package net.rizon.acid.events;

import net.rizon.acid.core.AcidCore;
import net.rizon.acid.core.Server;
import net.rizon.acid.core.User;

/**
 * Class that contains data required to handle ENCAP events.
 *
 * @author orillion <orillion@rizon.net>
 */
public class EventEncap
{
	private final Server originServer;
	private final User originUser;
	private final Server target;
	private final String command;
	private final String[] arguments;

	/**
	 * Constructs a new EventEncap object.
	 *
	 * @param originServer Server the ENCAP originated from, or null if from
	 *                     user.
	 * @param originUser   User the ENCAP originated from, or null if from
	 *                     server.
	 * @param target       Which server the ENCAP is targeted at, or null if
	 *                     targeted at all.
	 * @param command      Command of the ENCAP.
	 * @param arguments    Arguments of the ENCAP.
	 */
	public EventEncap(Server originServer, User originUser, Server target, String command, String[] arguments)
	{
		this.originServer = originServer;
		this.originUser = originUser;
		this.target = target;
		this.command = command;
		this.arguments = arguments;
	}

	/**
	 * Returns the origin of the ENCAP, if any.
	 *
	 * @return Origin server, or null if not sent by a server.
	 */
	public Server getOriginServer()
	{
		return originServer;
	}

	/**
	 * Returns the user who sent the ENCAP, if any.
	 *
	 * @return Origin user, or null if not sent by a user.
	 */
	public User getOriginUser()
	{
		return originUser;
	}

	/**
	 * Returns the target for ENCAP
	 *
	 * @return Server if specific target, null otherwise.
	 */
	public Server getTarget()
	{
		return target;
	}

	/**
	 * Returns the ENCAP command
	 *
	 * @return Command
	 */
	public String getCommand()
	{
		return command;
	}

	/**
	 * Returns the list of arguments for this ENCAP
	 *
	 * @return Arguments
	 */
	public String[] getArguments()
	{
		return arguments;
	}

	public boolean isTargettedAtMe()
	{
		return this.target == null || this.target == AcidCore.me;
	}
}
